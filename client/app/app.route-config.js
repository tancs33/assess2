(function () {  
    
angular
.module("groceryApp")
.config(uirouterAppConfig);
uirouterAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

function uirouterAppConfig($stateProvider,$urlRouterProvider){
$stateProvider
.state("home",{ 
    url : "/home",
    templateUrl: "home.html",
    controller : 'groceryController', 
    controllerAs : 'ctrl'
})
.state("edit", {
    url: "/edit",
    templateUrl: "editpage.html",
    controller : 'groceryController',
    controllerAs : 'ctrl'
})
.state("add", {
    url: "/add",
    templateUrl: "addpage.html",
    controller : 'AddCtrl',
    controllerAs : 'ctrl'
})

$urlRouterProvider.otherwise("/home"); 

}

})();