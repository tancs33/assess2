(function () {
    "use strict";
    angular.module("groceryApp")
    .controller("groceryController", groceryController)
    .controller("EditCtrl", EditCtrl)
    .controller("AddCtrl", AddCtrl);

    groceryController.$inject = ['groceryAppAPI', '$uibModal', '$document', '$rootScope', '$scope', '$state'];
    EditCtrl.$inject = ['groceryAppAPI', 'items', '$document', '$uibModalInstance', '$rootScope', '$scope', '$state'];
    AddCtrl.$inject = ['groceryAppAPI', '$rootScope', '$scope', '$state'];

    function EditCtrl(groceryAppAPI, items, $document, $uibModalInstance, $rootScope, $scope, $state) {
        console.log("In EditCtrl...")

        var self = this;
        self.items = items;
        console.log(items);

        groceryAppAPI.getGrocery(items).then(function(result) {
                console.log(result.data);
                self.grocery =  result.data;
        });

        self.saveGrocery = saveGrocery;
        self.cancelGrocery = cancelGrocery;

        function saveGrocery() {
               console.log("Saving... ");
               console.log(self.grocery.upc12);
               console.log(self.grocery.brand);
               console.log(self.grocery.name);
              
               groceryAppAPI.updateGrocery(self.grocery).then(function(result){ 
                   console.log(result);
                   $rootScope.$broadcast('updateGrocery', {upc12: result.data.upc12}); 
               }).catch(function(error){
                   console.log(error);
               });

               $uibModalInstance.close(self.run);
        };

        function cancelGrocery() {
            $uibModalInstance.close(self.run);
        };
    }

    function AddCtrl(groceryAppAPI, $rootScope, $scope, $state) { 
        console.log("In AddCtrl");
        var self = this;

        self.addNewGrocery = addNewGrocery;
     
        function addNewGrocery() {
            console.log("Adding... ");
            console.log(self.grocery.upc12);
            console.log(self.grocery.brand);
            console.log(self.grocery.name);

            self.status = {
                message: ""
            }; 
           
            groceryAppAPI.addGrocery(self.grocery).then(function(result){ //update here to match service.js
                console.log(result);
                self.status.message = self.grocery.brand + " " + self.grocery.name + "  has been succesfully added"
            }).catch(function(error){
                console.log(error);
                self.status.message = "Failed to add";
            })   
        }


    }

    function groceryController(groceryAppAPI, $uibModal, $document, $rootScope, $scope, $state) {
        var self = this;

        self.searchGrocery = searchGrocery;
        self.editItem = editItem;
        self.pageChanged = pageChanged;

        self.grocery = []; 

        
        self.maxsize = 5; 
        self.itemsPerPage = 20;
        self.totalItems = 0; 
        self.currentPage = 1;

        function pageChanged() {
            console.log("Page changed " + self.currentPage);

            groceryAppAPI.searchGrocery(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage).then(function(results){ 
                console.log(results);
                console.log(results.data.rows);
                self.grocery = results.data.rows;
                self.totalItems = results.data.count;
            }).catch(function (error) {
                console.log(error);
            }); 
        }


        function searchGrocery() {
            console.log("Searching...");

            groceryAppAPI.searchGrocery(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage).then(function(results){ 
                console.log(results);
                console.log(results.data.rows);
                self.grocery = results.data.rows;
                self.totalItems = results.data.count;
            }).catch(function (error) {
                console.log(error);
            }); 
        };

        $scope.$on("updateGrocery", function() { 
            console.log("Refresh grocery list " + self.searchKeyword);
         
            groceryAppAPI.searchGrocery(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage).then(function(results){ 
                console.log(results);
                self.grocery = results.data.rows;
                self.totalItems = results.data.count;
            }).catch(function (error) {
                console.log(error);
            });  

        });


        function editItem(upc12, size, parentSelector) {
            console.log("Editing");
            console.log("Barcode is " + upc12);
           
        //MODAL    
        var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: self.animationsEnabled, 
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'edit.html', 
            controller: 'EditCtrl', 
            controllerAs: 'ctrl', 
            size: size,
            appendTo: parentElem,
            resolve: {
              items: function () {
                return upc12;
              }
            }
          }).result.catch(function(resp) {
            if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)
                throw resp; 
          });
        };

    }
       


})(); 