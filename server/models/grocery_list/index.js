
module.exports = function(connection, Sequelize){ 
    
        var Grocery = connection.define('grocery_lists', { 
            id: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            upc12: {
                type: Sequelize.INTEGER(12),
                allowNull: false,
            },
            brand: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false,
            }
        }, 
            {
                timestamps: false 
    
        });  
        return Grocery; 
    }